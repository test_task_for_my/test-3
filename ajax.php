<?php
require __DIR__ . '/config.php';
require __DIR__ . '/vendor/autoload.php';


if( ! Dump::isAjax()){
	exit('Не Ajax запрос');
}else{
	$json = [];

	$sizePage = 10;
	$list_images = [];
	$offset = $sizePage * ($_GET['page'] - 1);
	//Dump::log('LIMIT ' . $offset . ',' . $sizePage);
	$list_images_res = DBClient::getInstance()->select('images', '', [], '*', ' LIMIT ' . $offset . ',' . $sizePage);
	//Dump::log('$list_images_res: ' . print_r($list_images_res, true));
	if( ! empty($list_images_res)) foreach($list_images_res as $img){
		if(file_exists(__DIR__ . '/images/' . $img['image_small']) and file_exists(__DIR__ . '/images/' . $img['image'])){
			$list_images[] = $img;
		}
	}
	//$list_images = $list_images_res;

	$json['listImages'] = $list_images;

	exit(json_encode($json));
}