let buffer;

/**
 * смена страницы
 */
changePage = (page) => {
	// console.log('el:', this);
	let el = document.getElementsByClassName('page-' + page);
	let els = document.getElementsByClassName('gallery-pages-item');

	if (isset(els)) if (!empty(els[0])) for (i = 0; i < els.length; i++) {
		els[i].classList.remove('active');
	}

	if (isset(el)) if (!empty(el[0])) el[0].classList.add('active');

	myAjax('/ajax.php?page=' + page);
	let content = buffer;
	if (!isset(content)) {
		console.log('content undefined');
	} else if (empty(content)) {
		console.log('EMPTY content: ', content);
	} else {

		json = JSON.parse(content);
		let html = '';
		let del = "\'";
		if (isset(json))
			if (isset(json.listImages))
				if (!empty(json.listImages)) {

					json.listImages.forEach((img) => {
						html +=
							'<div class="gallery-item">' +
							'<img src="/images/' + img.image_small + '" ' +
							'onclick="zoom(' + del + img.name_in + del + ',' + del + img.brief_me + del +
							', ' + del + '/images/' + img.image + del + ')" alt="" title="">' +
							'</div>';
					});
					html += '<div class="clearfix"></div>';
				} else {
					html = '<p class="alert alert-info text-center">Для этой страницы не найдено ни одной существующей картинки</p>';
				}
		let gallery = document.getElementById('gallery-items');
		gallery.innerHTML = html;
	}

};


/**
 * какрытие модального окна
 */
closeModal = () => {
	let galleryModal = document.getElementById('gallery-modal');
	galleryModal.classList.remove('block');
	galleryModal.innerHTML = '';
};

/**
 * показ большой картинки
 */
zoom = (name_in, brief_me, big_image) => {
	let galleryModal = document.getElementById('gallery-modal');
	galleryModal.innerHTML = '<div class="gallery-modal-content"><img onclick="closeModal()" src="' + big_image + '">' +
		'<p>' + brief_me + '</p>' +
		'<p>' + big_image + '</p></div>';
	galleryModal.classList.add('block');
};


/**
 * В задании сказано нельзя jQuery поэтому тут этот велосипед
 */
myAjax = (url) => {
	let request = new XMLHttpRequest();
	request.open('AJAX', url, false);

	// request.ajax('/ajax.php');

	request.onload = (answer) => {
		if (answer.currentTarget.status >= 200 && answer.currentTarget.status < 400) {
			// Success!
			buffer = answer.currentTarget.response;
			return buffer;
		} else {
			return null;
		}
	};

	// request.onerror = function () {
	// 	// There was a connection error of some sort
	// };
	request.send();
};

/**
 * аналог php функци count()
 *
 * @return int
 */
function count(c) {
	let a = 0, b;
	for (b in c) b && a++;
	return a
}

function isset(val) {
	return typeof val !== 'undefined';
}


function empty(val) {
	if (typeof val === 'undefined') return true;
	return val.length === 0;
}