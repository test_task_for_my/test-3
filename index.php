<?php
require __DIR__ . '/config.php';
require __DIR__ . '/vendor/autoload.php';

$sizePage = 10;
$list_images = [];
$offset = $sizePage * 0;
$list_images_res = DBClient::getInstance()->select('images', '', [], '*', ' LIMIT ' . $offset . ',' . $sizePage);
if( ! empty($list_images_res)) foreach($list_images_res as $img){
	if(file_exists(__DIR__ . '/images/' . $img['image_small']) and file_exists(__DIR__ . '/images/' . $img['image'])){
		$list_images[] = $img;
	}
}
// Собираем пагинацию
$pagination = DBClient::getInstance()->getPageNav('images', $sizePage, 'product_id');

require __DIR__ . '/tmpl/page.php';
