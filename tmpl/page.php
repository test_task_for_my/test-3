<?php

?>
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="/vendor/twbs/bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" href="/assets/css/style.css">
	<script type="text/javascript" src="/assets/js/common.js"></script>
	<title>Тестовое задание 1</title>
</head>
<body>
<div class="container">
	<div class="row">
		<div class="col-12">
			<? // Dump::xp($list_images, 1, '$list_images');
			if(empty($list_images)): ?>
				<p class="alert alert-info">Пустой список продуктов</p>
			<? else: ?>
				<div id="gallery" class="gallery">
					<div id="gallery-items">
						<? foreach($list_images as $img): ?>
							<div class="gallery-item">
								<img src="/images/<?=$img['image_small']?>" onclick="zoom('<?=$img['name_in']?>','<?=$img['brief_me']?>','/images/<?=$img['image']?>')" alt="" title="">
							</div>
						<? endforeach ?>
						<div class="clearfix"></div>
					</div>
					<div class="gallery-pages">
						<? for($i = 0; $i < $pagination['count_pages']; $i ++): ?>
							<span class="gallery-pages-item page-<?=$i + 1?> <?=($i === 0) ? 'active' : ''?>" onclick="changePage(<?=$i + 1?>)"> <?=$i + 1?> </span>
						<? endfor ?>
					</div>
				</div>
			<? endif ?>
		</div>
	</div>
</div>
<div class="gallery-modal" id="gallery-modal"></div>

<script type="text/javascript" src="/vendor/libra/jquery-assets/public/js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="/vendor/twbs/bootstrap/dist/js/bootstrap.min.js"></script>

</body>
</html>
